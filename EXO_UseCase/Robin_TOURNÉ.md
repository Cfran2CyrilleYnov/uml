Tâches
En tant qu’acheteur je veux consulter le catalogue produit afin de choisir ma commande

En tant qu’acheteur je veux enregistrer un achat afin de d’acheter un article

En tant qu’acheteur, je veux constituer un panier afin de constituer ma commande

En tant qu’acheteur, je veux saisir les informations pour livraison afin de recevoir ma commande ou je le souhaite

En tant que Système bancaire, je veux enregistrer un achat, afin de pouvoir procéder au paiement

En tant que livreur je veux préparer un livraison afin de livrer les clients

En tant que Technicien je veux consulter une remarque afin d’améliorer le service/résoudre d’éventuels problèmes techniques
