
Michael LEVESQUE - B2 informatique

User Story : Système de livraison 

 

*** 

En tant que client je veux pouvoir accéder aux informations de ma commande 

En tant que client je veux que les informations nécessaires de livraison soit protégée 

En tant que client je veux consulter l’historique de mes achats 

En tant que client je veux pouvoir m’authentifier pour disposer d’un compte, et d’un panier sauvegarder 

En tant que client je veux avoir accès aux suivis de livraison 

 

*** 

En tant que commercial je veux consulter l’ensemble des produits à disposition des clients 

En tant que commercial je veux pouvoir sélectionner le bénéficiaire de la commande afin d’avoir ses infos pour la livraison 

En tant que commercial je veux avoir accès au système bancaire pour permettre les différentes transactions 

 

*** 

En tant que livreur je veux consulter les informations du client pour lui préparer sa commande 

En tant que livreur je veux avoir accès à la localisation du produit pour pouvoir le trouver le plus rapidement possible 

 

*** 

En tant que technicien je veux consulter les comptes des clients afin de pouvoir permettre une maintenance sur leurs comptes en cas de problème technique 

En tant que technicien je veux avoir accès à l'ensemble du système informatique afin de pouvoir repérer et réparer une quelconque panne éventuelle 

 

 